const cheerio = require('cheerio')

module.exports = {
  productcounts: function (page) {
    const $ = cheerio.load(page)
    return $('#promolain_inside').siblings().length
  },
  subcatcounts: function (page) {
    const $ = cheerio.load(page)
    return $('#subcatpromo').children().length
  },
  unique: function (arr, propname) {
    var flag = {}
    return arr.filter(entry => {
      if (flag[entry[propname]]) {
        return false
      }
      flag[entry[propname]] = true
      return true
    })
  },
  paging: function (page) {
    const $ = cheerio.load(page)
    const children = $('.tablepaging tbody tr').children()
    const res = []
    children.each(function (i, elem) {
      res[i] = {}
      res[i].product = $(elem).find('a').attr('product')
      res[i].subcat = $(elem).find('a').attr('subcat')
      res[i].page = $(elem).find('a').attr('page')
    })
    const resuniq = this.unique(res, 'page')
    return resuniq
  },
  promolist: function (page) {
    const $ = cheerio.load(page)
    const pagecontent = $('#promolain li')
    const res = []
    pagecontent.each(function (i, elem) {
      res[i] = {}
      res[i]['url'] = $(elem).find('a').attr('href')
      res[i]['title'] = $(elem).find('a').find('img').attr('title')
    })
    return res
  },
  producttitle: function (page) {
    const $ = cheerio.load(page)
    return $('.menupromoinside').text()
  },
  subcattitle: function (page) {
    const $ = cheerio.load(page)
    const elem = $('#subcatselected')
    const title = $(elem).find('img').attr('title')
    return title
  },
  promoinformation: function (page, mainurl) {
    const $ = cheerio.load(page)
    const elem = $('body #wrapperindex #contentpromolain2')
    const information = {}
    information.title = $(elem).find('.titleinside').find('h3').text()
    information.area = $(elem).find('.area').find('b').text()
    information.periode = $(elem).find('.periode').find('b').text()
    information.keterangan = mainurl + $(elem).find('.keteranganinside').find('img').attr('src')
    return information
  }
}
