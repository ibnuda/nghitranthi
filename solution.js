const ex = require('./extraction')

const down = require('./down')

const url = require('./urls')

const fs = require('fs')

// get product pages on url.main/url.lainnya?product=x
// url retrieved from url.main/url.lainnya
// accepts cherio
// returns {'page': cherio, 'productid': x, 'title': something}
const getproductpages = async page => {
  const pagecount = ex.productcounts(page)
  const pages = []
  for (var i = 0; i < pagecount; i++) {
    pages[i] = {}
    pages[i].page = await down.page(url.product(i + 1))
    pages[i].title = ex.producttitle(pages[i].page)
    pages[i].productid = i + 1
  }
  return pages
}

// get product pages on url.main/url.lainnya?product=x&subcat=y
// url retrieved from url.main/url.lainnya
// accepts {'page': cherio, 'productid': x, 'title': something}
// returns {productid: x, title: something, subcat: [sub]}
const getsubcatpages = async resproductpage => {
  const subcatcounts = ex.subcatcounts(resproductpage.page)
  const subcat = []
  if (subcatcounts === 0) {
    const page = await down.page(url.subcat(resproductpage.productid, ''))
    subcat[0] = {}
    subcat[0].pages = ex.paging(page)
    subcat[0].pid = resproductpage.productid
    subcat[0].title = resproductpage.title
  } else {
    for (var i = 0; i < subcatcounts; ++i) {
      const page = await down.page(url.subcat(resproductpage.productid, i + 1))
      subcat[i] = {}
      subcat[i].title = ex.subcattitle(page)
      subcat[i].pages = ex.paging(page)
    }
  }
  return subcat
}

const getlistpromo = async (title, pages) => {
  const specificpages = await Promise.all(pages.map(p => {
    const pid = p.product
    const sid = p.subcat
    const pag = p.page
    const u = url.page(pid, sid, pag)
    return down.page(u)
  }))
  const res = {}
  res.title = title
  // const cats = productsubcatpages.reduce((acc, val) => acc.concat(val), [])
  const result = specificpages.map(p => {
    return ex.promolist(p)
  })
  res.result = result.reduce((acc, val) => acc.concat(val), [])
  return res
}

const getsinglepromo = async r => {
  const promourl = url.mainurl + '/' + r.url
  const page = await down.page(promourl).catch({})
  return ex.promoinformation(page, url.mainurl)
}

const getpromos = async (title, res) => {
  const result = {}
  result.title = title
  result.promoinfos = await Promise.all(res.filter(r => !r.url.startsWith('http')).map(getsinglepromo)).catch({})
  return result
}

const formatasrequested = arres => {
  const result = {}
  arres.forEach(res => {
    result[res.title] = res.promoinfos
  })
  return result
}

// just found out that async/await is something like monad.
const mainentry = async () => {
  const mainpage = await down.page(url.mainurl + '/' + url.otherpromo)
  const productpages = await getproductpages(mainpage)
  const productsubcatpages = await Promise.all(productpages.map(getsubcatpages))
  const cats = productsubcatpages.reduce((acc, val) => acc.concat(val), [])
  const flattenedcats = await Promise.all(cats.map(cat => getlistpromo(cat.title, cat.pages)))
  const promourls = await Promise.all(flattenedcats.map(cat => getpromos(cat.title, cat.result)))
  const formatted = formatasrequested(promourls)
  fs.writeFile('solution.json', JSON.stringify(formatted, null, 2), x => {})
}

mainentry()
