module.exports = {
  page: async function (url) {
    return new Promise((resolve, reject) => {
      const lib = url.startsWith('https') ? require('https') : require('http')
      var options = {
        'header': {
          'Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0',
          'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
          'Connection': 'keep-alive'
        }
      }
      const req = lib.get(url, options, resp => {
        if (resp.statusCode < 200 || resp.statusCode > 299) {
          console.log('url: ' + url + ' rewel.')
          reject(new Error('Failed to load page. Status code: ' + resp.statusCode))
        }
        const body = []
        resp.on('data', chunk => body.push(chunk))
        resp.on('end', () => resolve(body.join('')))
        req.on('error', err => reject(err))
      })
    })
  }
}
