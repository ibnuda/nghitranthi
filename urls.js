module.exports = {
  mainurl: '',
  otherpromo: '',
  product: function (pid) {
    return (this.mainurl + '/' + this.otherpromo + '?product=' + pid)
  },
  subcat: function (pid, sid) {
    return (this.product(pid) + '&subcat=' + sid)
  },
  page: function (pid, sid, p) {
    const something = this.subcat(pid, sid) + '&page=' + p
    return something
  },
  page2: function (pid, sid, p) {
    return (this.mainurl + '/ajax.' + this.otherpromo + '?product=' + pid + '&subcat=' + sid + '&page=' + p)
  }
}
